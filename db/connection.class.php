<?php
/*connection.class*/

	class Connection{

		var $type;
		var $connection;
		var $connected = false;

		function __construct($type = "master") {
			$this->type = $type;
		}

		function __destruct() {
			$this->disconnect();
		}

		function connect(){
			global $dbconfig;

			$this->connection = mysqli_connect($dbconfig[$this->type]["dbhost"],
			'root',
			$dbconfig[$this->type]["dbpass"]) or die("connection error:"
			.$dbconfig[$this->type]["dbname"] . " - ");
			mysqli_select_db($this->connection, $dbconfig[$this->type]["dbname"]);

			mysqli_query($this->connection, "SET time_zone = '".date_default_timezone_get()."'");
			mysqli_query($this->connection, "SET CHARACTER SET 'utf8'");
			mysqli_query($this->connection, "SET NAMES utf8;");

			$this->connected = true;
		}

		function query($query){

			if(!$this->connected){
				$this->connect();
			}

			return mysqli_query($this->connection, $query);//sorgudan dönen listeyi döndür
		}

		function getQueryResult($query,$type="ARRAY"){
			$result = $this->query($query);

			$toReturn = array();
			if($result){
				while($row = mysqli_fetch_assoc($result)){//while ile hepsini yazdırıyoruz
					$toReturn[] = $row;
				}
			}

			if($type == "JSON"){
				return json_encode($toReturn);//diziden bilgileri yazdırıyor
			}

			return $toReturn;
		}

		function getQueryResultFirstItem($query,$type="ARRAY"){//İlk itemi yazdır
			$result = $this->query($query);

			$toReturn = array();
			if(!$result)
				return $toReturn;

			if($row = mysqli_fetch_assoc($result)){
				$toReturn = $row;
			}

			if($type == "JSON"){
				return json_encode($toReturn);
			}

			return $toReturn;
		}

		function escape($toEscape){//Stringlerin boşluklarını yok et
			if(!$this->connected){
				$this->connect();
			}
			if(is_array($toEscape))
				$toEscape = trim(implode(" ",$toEscape));
			return mysqli_real_escape_string($this->connection,$toEscape);
		}

		function disconnect(){//Database bağlantısını kapat
			if($this->connected){
				mysqli_close($this->connection);
				$this->connected = false;
			}
		}

		function buildQuery($type = "insert",$table,$args=array(),$conditions=array()){

			$query = "";

			if($type == "insert"){//ilk giriş tarihi
				$keys 	= "";
				$values	= "";
				foreach($args as $key => $value){//Value değerlerini biçimlendirme

					$keys .= ($keys != "" ? "," : "") . sprintf("`%s`",self::escape($key));

					$quot = ($value == "CURRENT_TIMESTAMP" || $value == "NOW()") ? "" : "'";

					if(substr($value, 0, 5) == 'SQL::'){
						$value = str_replace('SQL::', '', $value);
						$quot = '';
						$values .= ($values != "" ? "," : "") . $value;
					}else{
						$values .= ($values != "" ? "," : "") . sprintf("$quot%s$quot",self::escape($value));
					}

				}
				$query = "INSERT INTO ".$table." (".$keys.") VALUES (".$values.")";//SQL insert into
			}
			else if($type == "update"){//giriş güncelleme tarih
				$updateFields = "";


				foreach($args as $key => $value){

					if($updateFields != ""){
						$updateFields .= ", ";
					}

					if(substr($value, 0, 5) == 'SQL::'){
						$value = str_replace('SQL::', '', $value);
						$updateFields .= "`$key` = $value";
					}
					else{

						$quot = ($value == "CURRENT_TIMESTAMP" || $value == "NOW()") ? "" : "'";

						$updateFields .= sprintf("`%s` = $quot%s$quot",self::escape($key),self::escape($value));
					}
				}


				$conditionFields = "";
				foreach($conditions as $key => $value){
					if($conditionFields != "")
						$conditionFields .= " AND ";
					$conditionFields .= sprintf("`%s` = '%s'",self::escape($key),self::escape($value));
				}
				if($conditionFields != "")
					$conditionFields = " WHERE " . $conditionFields;

				$query = "UPDATE " . $table . " SET " . $updateFields . $conditionFields;//sql update cümlecik
			}
			else if($type == "delete"){
				$conditionFields = "";
				foreach($conditions as $key => $value){
					if($conditionFields != "")
						$conditionFields .= " AND ";
					$conditionFields .= sprintf("`%s` = '%s'",self::escape($key),self::escape($value));
				}
				if($conditionFields != "")
					$conditionFields = " WHERE " . $conditionFields;

				$query = "DELETE FROM " . $table . $conditionFields;//sql delete cümlecik
			}

			return $query;
		}
		function insertId(){
			return mysqli_insert_id($this->connection);
		}
		function affectedRows(){
			return mysqli_affected_rows($this->connection);
		}
	}

	if(empty($connection) || !is_object($connection))
		$connection = new Connection();

/*
	class Connection{
		
		var $type;
		var $connection;
		var $connected = false;
		
		function __construct($type = "master") {
			$this->type = $type;
		}
		
		function __destruct() {
			$this->disconnect();
		}
		
		function connect(){
			global $dbconfig;

			$this->connection = mysql_connect($dbconfig[$this->type]["dbhost"], $dbconfig[$this->type]["dbuser"], $dbconfig[$this->type]["dbpass"]) or die("connection error:" .$dbconfig[$this->type]["dbname"] . " - ");	
			mysql_select_db($dbconfig[$this->type]["dbname"], $this->connection);
	
			mysql_query("SET CHARACTER SET 'utf8'");
			mysql_query("SET NAMES utf8");

			$this->connected = true;
		}
		
		function query($query){

			if(!$this->connected){
				$this->connect();
			}

			return mysql_query($query,$this->connection);

		}
		
		function insertId(){
			return mysql_insert_id($this->connection);
		}

		function getQueryResult($query,$type="ARRAY"){
			$result = $this->query($query);

			$toReturn = array();	
			if($result){
				while($row = mysql_fetch_assoc($result)){
					$toReturn[] = $row;
				}
			}
			
			if($type == "JSON"){
				return json_encode($toReturn);
			}
			
			return $toReturn;
		}
		
		function getQueryResultFirstItem($query,$type="ARRAY"){
			$result = $this->query($query);
			
			$toReturn = array();
			if(!$result)
				return $toReturn;
				
			if($row = mysql_fetch_assoc($result)){
				$toReturn = $row;
			}
			
			if($type == "JSON"){
				return json_encode($toReturn);
			}
			
			return $toReturn;
		}

		function escape($toEscape){
			if(!$this->connected){
				$this->connect();
			}
			if(is_array($toEscape))
				$toEscape = trim(implode(" ",$toEscape));
			return mysql_real_escape_string($toEscape,$this->connection);
		}
		
		function disconnect(){
			if($this->connected){
				mysql_close($this->connection);
				$this->connected = false;
			}
		}
		
		function buildQuery($type = "insert",$table,$args=array(),$conditions=array()){
			
			$query = "";
			
			if($type == "insert"){ 
				$keys 	= "";
				$values	= "";
				foreach($args as $key => $value){
					$keys .= ($keys != "" ? "," : "") . sprintf("`%s`",self::escape($key));
					
					$quot = ($value == "CURRENT_TIMESTAMP" || $value == "NOW()") ? "" : "'";
					$values .= ($values != "" ? "," : "") . sprintf("$quot%s$quot",self::escape($value));
				}
				$query = "INSERT INTO ".$table." (".$keys.") VALUES (".$values.")";
			}
			else if($type == "update"){
				$updateFields = "";
				foreach($args as $key => $value){
					if($updateFields != "")
						$updateFields .= ", ";
					$updateFields .= sprintf("`%s` = '%s'",self::escape($key),self::escape($value));
				}
				$conditionFields = "";
				foreach($conditions as $key => $value){
					if($conditionFields != "")
						$conditionFields .= " AND ";
					$conditionFields .= sprintf("`%s` = '%s'",self::escape($key),self::escape($value));
				}
				if($conditionFields != "")
					$conditionFields = " WHERE " . $conditionFields;
					
				$query = "UPDATE " . $table . " SET " . $updateFields . $conditionFields;
			}
			else if($type == "delete"){ 
				$conditionFields = "";
				foreach($conditions as $key => $value){
					if($conditionFields != "")
						$conditionFields .= " AND ";
					$conditionFields .= sprintf("`%s` = '%s'",self::escape($key),self::escape($value));
				}
				if($conditionFields != "")
					$conditionFields = " WHERE " . $conditionFields;
					
				$query = "DELETE FROM " . $table . $conditionFields;
			}
			
			return $query;
		}
		
		function affectedRows(){ 
			return mysql_affected_rows($this->connection);
		}
	}
	$connection = new Connection();
?>*/