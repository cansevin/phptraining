<?php

	require_once ("connection.class.php");

	class User{
		public static function get($id){

			global $connection;

			$toReturn = array("result" => false, "messages" => array());
			if(sizeof($toReturn["messages"]) == 0){
				$query = "SELECT
							c.*
						FROM
							tbl_users AS c
						WHERE c.id = $id";
				$toReturn["data"] = $connection->getQueryResultFirstItem($query);
				$toReturn["result"] = !empty($toReturn["data"]);
			}
			return $toReturn;
		}

		public static function add($args){
			global $connection;

			$toReturn = array("result" => false, "messages" => array());

			if(empty($args["title"])) $toReturn["messages"][] = "Title ?";

			if(empty($toReturn["messages"])){

				$query = $connection->buildQuery("insert", "tbl_users", $args);
				$toReturn["result"] = $connection->query($query);

				if($toReturn["result"]){
					$toReturn["insert_id"] = $connection->insertId();
				}
				else{
					$toReturn["query"] = $query;
				}

			}
			return $toReturn;
		}
		public static function update($args, $id){

			global $connection;

			$toReturn = array("result" => false, "messages" => array());

			if(empty($args["company_name"]) && empty($args["person_first_name"])) $toReturn["messages"][] = "Name ?";

			if(empty($toReturn["messages"])){
				$query = $connection->buildQuery("update", "tbl_customers", $args, array("id" => $id));
				$toReturn["result"] = $connection->query($query);
				if($toReturn["result"]){
					$toReturn["messages"][] = "Kullanıcı güncellendi";
				}
				else{
					$toReturn["q"] = $query;
				}
			}

			return $toReturn;
		}
		public static function delete($id){

			global $connection;

			$toReturn = array("result" => false, "messages" => array());

			$id = (int) $id;

			$toReturn["messages"][] = "Closed...";

			if(empty($toReturn["messages"])){
				$toReturn["result"] = $connection->query("DELETE FROM tbl_customers WHERE id = $id");
			}

			return $toReturn;
		}
	}