<?php

	require_once("connection.class.php");

	class Customer{
		public static function search($args = array(), $page = 0, $limit = 50,$sort_key = "c.id", $sort_direction = "DESC"){
			global $connection;

			$toReturn = array("result" => true, "messages" => array());

			$conditionFields = " WHERE cm.id = (SELECT id FROM tbl_cm_history WHERE customer_id = c.id ORDER BY id DESC LIMIT 1) AND s.id = cm.status AND cm.customer_id = c.id";

			$status_table = "tbl_cm_statuses";

			if(!empty($args)){

				foreach($args as $key => $value){

					$conditionFields .= " AND ";

					if(substr($key, -3) == '_id' || substr($key, -3) == '.id' || $key == 'cm.status'){

						if(is_array($value)){
							$conditionFields .= "$key IN (".implode(",", $value).")";
						}
						else{
							$conditionFields .= sprintf("%s = '%s'",$connection->escape($key),$connection->escape($value));
						}
					}
					else if($key == 'SQL::'){
						$conditionFields .= $value;
					}
					else{
						$conditionFields .= sprintf("%s like '%%%s%%'",$connection->escape($key),$connection->escape($value));
					}
				}
			}

			$limit_query = sprintf("LIMIT %s,%s", $connection->escape($page * $limit), $connection->escape($limit));
			$sort_query = sprintf(" ORDER BY %s %s",$connection->escape($sort_key),$connection->escape($sort_direction));

			$count_query = "SELECT
								COUNT(c.id) as cnt
							FROM
								tbl_customers AS c,
								tbl_cm_history AS cm,
								$status_table AS s
							$conditionFields";

			$total_result = $connection->getQueryResultFirstItem($count_query); //Gets first element
			$total = 0;

			$result = array();

			if(!empty($total_result)){
				$total = $total_result["cnt"];

				$query = "SELECT
								c.*,
								DATE_FORMAT(c.created_at, '%d-%m-%Y %H:%i') AS created_at_formatted,
								s.title AS status_title
							FROM
								tbl_customers AS c,
								tbl_cm_history AS cm,
								$status_table AS s
							$conditionFields
							$sort_query
							$limit_query";

				$toReturn["q"] = $query;

				$result = $connection->getQueryResult($query);

			}

			$toReturn["data"] = $result;
			$toReturn["result"]		 = sizeof($toReturn["result"]);
			$toReturn["count"]		 = sizeof($toReturn["data"]);
			$toReturn["total"]		 = $total;
			$toReturn["page"]		 = $page;
			$toReturn["total_pages"] = ceil($total/$limit);

			return $toReturn;
		}

		public static function get($id){

			global $connection;
			$toReturn = array("result" => false, "messages" => array());

			if(sizeof($toReturn["messages"]) == 0){
				$query = "SELECT
							c.*FROM
							tbl_customers AS c
						WHERE c.id = $id";

				$toReturn["data"] = $connection->getQueryResultFirstItem($query);
				$toReturn["result"] = !empty($toReturn["data"]);
			}

			return $toReturn;
		}
		public static function add($args){

			global $connection;

			$toReturn = array("result" => false, "messages" => array());

			if(empty($args["title"])) $toReturn["messages"][] = "Title ?";

			if(empty($toReturn["messages"])){

				$query = $connection->buildQuery("insert", "tbl_customers", $args);
				$toReturn["result"] = $connection->query($query);

				if($toReturn["result"]){
					$toReturn["insert_id"] = $connection->insertId();
				}
				else{
					$toReturn["query"] = $query;
				}

			}
			return $toReturn;
		}
		public static function update($args, $id){

			global $connection;

			$toReturn = array("result" => false, "messages" => array());

			if(empty($args["company_name"]) && empty($args["person_first_name"])) $toReturn["messages"][] = "Name ?";

			if(empty($toReturn["messages"])){
				$query = $connection->buildQuery("update", "tbl_customers", $args, array("id" => $id));
				$toReturn["result"] = $connection->query($query);
				if($toReturn["result"]){
					$toReturn["messages"][] = "Customer updated..";
				}
				else{
					$toReturn["q"] = $query;
				}
			}

			return $toReturn;
		}
		public static function delete($id){

			global $connection;

			$toReturn = array("result" => false, "messages" => array());

			$id = (int) $id;

			$toReturn["messages"][] = "Closed...";

			if(empty($toReturn["messages"])){
				$toReturn["result"] = $connection->query("DELETE FROM tbl_customers WHERE id = $id");
			}

			return $toReturn;
		}
	}