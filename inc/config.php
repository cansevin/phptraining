<?php

error_reporting( E_ALL );
ini_set('display_errors', true);

define('SITE', 'IDENTIFY_ADMIN');
define('LOCAL', false);

define('COOKIE_TIME', 1296000);
define('COOKIE_DOMAIN', 'xxx.com');

$log_qry = false;
$url_rewrite = true;

//error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
//ini_set('display_errors', 1);

define('HTTPS', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? true : false);

$path_root = realpath(dirname(__FILE__).'/../');

if(HTTPS){
	$url_root = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
}
else{
	$url_root = 'http';
}

$url_fix = str_replace(realpath($path_root."/../"), '', $path_root);

$url_root = $url_root . "://" . $_SERVER['HTTP_HOST'].$url_fix;

//$path_media = $path_root.'/static/media';
$path_media = realpath($path_root.'/../static/media');

define('PATH_MEDIA', $path_media);

//$url_media = $url_root.'/static/media';
$url_media = $url_root.'/../static/media';
define('URL_MEDIA', $url_media);

$url_no_image = $url_root.'/static/media/images/no_image/';
define('URL_NO_IMAGE', $url_no_image);

$url_site = $url_root;
define('URL_SITE', $url_root);

$url_root = $url_root;
define('URL_ROOT', $url_root);

$path_root = $path_root;
define('PATH_ROOT', $path_root);

$path_template = "$path_root/tpl";
define('PATH_TEMPLATE', $path_template);
$path_template_cache = "$path_root/tpl/cache";

$path_cron = PATH_ROOT."/cron";
$path_php = "/usr/bin/php -q";

date_default_timezone_set('Europe/Berlin');

$timediff = 0;
define('TIME_DIFF', 0);

$timer = microtime(true);
$time = time() + TIME_DIFF;
$current_time = date('Y-m-d H:i:s', $time);
$timestamp_now = date('YmdHis', $time);
$timestamp_expire = date('YmdHis', $time - 1800); // 30 minutes

$url_no_image = '';
define(URL_NO_IMAGE, $url_no_image);

$dbconfig = array();
$dbconfig["master"]["dbhost"] = 'localhost';
$dbconfig["master"]["dbuser"] = 'root';
$dbconfig["master"]["dbpass"] = '';
$dbconfig["master"]["dbname"] = 'deneme';