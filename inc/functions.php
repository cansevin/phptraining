<?php

function generate_token($length){
	$token = "";
	$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
	$codeAlphabet.= "0123456789";
	$max = strlen($codeAlphabet);
	for ($i=0; $i < $length; $i++) {
		$token .= $codeAlphabet[random_int(0, $max-1)];
	}
	return $token;
}

function delete_folder($folder) {

	if(!$folder) return false;

	$glob = glob($folder);
	foreach ($glob as $g) {
		if (!is_dir($g)) @unlink($g);
		else {
			delete_folder("$g/*");
			@rmdir($g);
		}
	}
}


function is_admin($section = ''){

	if(empty($_SESSION['admin'])) return false;
	else{
		if($_SESSION['admin']['super_admin']) return true;
		else if($section) return $_SESSION['admin'][$section.'_admin'] == 1;
		else return false;
	}

	return false;
}

function money($number, $javascript=false, $decimal=true){
	$decimal = $decimal ? 2 : 0;
	return $javascript ? number_format($number, $decimal, '.', '') : number_format($number, $decimal, ',', '.');
}


function convert_url_de($string){
    $string = mb_strtolower($string, 'UTF-8');
    $string = html_entity_decode($string);
    $string = str_replace(array('ä','ü','ö','ß'),array('ae','ue','oe','ss'),$string);
    $string = preg_replace('#[^\w\säüöß]#',null,$string);
    $string = preg_replace('#[\s]{2,}#',' ',$string);
    $string = str_replace(array(' '),array('-'),$string);
    return $string;
}

function convert_url($url, $strict=true){
	$url = mb_strtolower($url, 'UTF-8');

	$replace = array('ı' => 'i', 'ç' => 'c', 'ş' => 's', 'ğ' => 'g', 'ö' => 'o', 'ü' => 'u');
	foreach($replace as $k => $v) $url = mb_eregi_replace($k, $v, $url);

	$url = trim(str_replace("'" ,'', $url));
	$url = $strict ? preg_replace('#[^a-zA-Z0-9]#', '-', $url) : preg_replace('#[^_a-zA-Z0-9]#', '-', $url);
	$url = preg_replace('#-+#', '-', $url);

	return trim($url, '-');
}

function date_convert($date){
	// In : 2003-04-25 or 25-04-2003 or 2003-04-25 16:52:20
	// Out: 25-04-2003 or 2003-04-25 or 25-04-2003 16:52:20
	$date=trim($date);
	if (strlen($date)>10) {
		$time=substr($date,10);
		$date=substr($date,0,10);
	}
	if (substr($date,4,1)=='-') {
		return preg_replace('#^([0-9]{2,4})-([0-9]{1,2})-([0-9]{1,2})$#', '\\3.\\2.\\1',$date);
	} else {
		return preg_replace('#^([0-9]{1,2}).([0-9]{1,2}).([0-9]{2,4})$#', '\\3-\\2-\\1',$date);
	}
}


function ajax_out($data){

	if (AJAX) {

		@header('Cache-Control: no-cache, must-revalidate');
		@header('Expires: Sat, 08 Mar 2000 17:30:00 GMT');

		if (is_array($data)) { // JSON
			@header('Content-Type: application/json; charset=utf-8');
			$data = json_encode($data);
			$data = str_replace(array("\t", "\r", "\n"), '', $data);
		} else {
			@header('Content-Type: text/html; charset=utf-8');
		}

		exit(trim($data));

	}
}


function set_json_header($status){

	$codes = Array(
		200 => 'OK',
		400 => 'Bad Request',
		401 => 'Unauthorized',
		402 => 'Payment Required',
		403 => 'Forbidden',
		404 => 'Not Found',
		500 => 'Internal Server Error',
		501 => 'Not Implemented',
	);

	$status_header = 'HTTP/1.1 ' . $status . ' ' . (in_array($status, $codes) ? $codes[$status] : '');
	$content_type="application/json; charset=utf-8";

	header($status_header);
	header('Content-type: ' . $content_type);
	header('X-Powered-By: ' . "xxx <xxx.com>");
}


function message($txt=''){
	if ($txt) {
		$_SESSION['message'] = $txt;
	} else {
		if(!empty($_SESSION["message"])){
			$message = $_SESSION['message'];
			unset($_SESSION['message']);
			return $message;
		}
	}
}


function log_this($contents, $type = 'debug'){

	global $current_time;

	$fp = fopen(PATH_ROOT."/log/$type.log", 'a');
	fputs($fp, "--[ $current_time ]------\n$contents\n");
	fclose($fp);
}


// alias for debug function
function d($data = '', $exit = true){return debug($data, $exit);}

function debug($data = '', $exit = true){

	if ($exit) @header('Content-Type: text/html; charset=utf-8');
	if ($data) echo '<pre>'.(is_array($data) ? sh(print_r($data, true)) : sh($data)).'</pre>';
	else echo 'Debug: '.date('H:i:s');
	if ($exit) exit;

}

function go($url, $header = 302){

	if(AJAX) ajax_out(array('go' => $url));

	if($header == 303){
		@header('HTTP/1.1 303 See Other');
	}
	elseif($header == 301){
		@header('HTTP/1.1 301 Moved Permanently');
	}

	@header('Location:'.$url, true, $header);
	exit;
}


function sh($var){

	return htmlspecialchars((string) $var);
	return htmlspecialchars($var, ENT_SUBSTITUTE, 'UTF-8');

}

function asb($array, $selected = ''){
	return array_select_box($array, $selected);
}

function array_select_box($array, $selected=''){
	if (!is_array($array)) return;

	$txt = '';

	if (is_array($selected)) {
		foreach ($array as $k => $v) {
			$sl = (($k && in_array($k, $selected)) || $k[$selected]) ? ' selected' : '';
			$txt .= "<option value=\"$k\"$sl>$v";
		}
	} else {
		foreach ($array as $k => $v) {
			$sl = ($k == $selected) ? ' selected' : '';
			$txt .= "<option value=\"$k\"$sl>$v";
		}
	}
	return $txt;
}